# Pico Synth - 'Synthesizer' program for the RaspberryPi Pico written in MicroPython

### Hardware used
* RaspberryPi Pico W microcontroller
* 12 push buttons keyboard
* 3 green LEDs indicating active voices
* 3 330Ω resistors as a primitive passive mixer
* Potentiometer in series with 2-pole RC low-pass filter (R = 10kΩ, C = 10nF)
* HXJ8002 power amplifier
* 3.5mm Jack TRRS module
