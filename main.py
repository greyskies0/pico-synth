"""Synth program for RPI Pico"""

# mypy: ignore-errors
from time import sleep
from machine import Pin, PWM # pylint: disable=import-error
import keys # pylint: disable=import-error


def init_keyboard_pins() -> []:
    """This function initializes the input keyboard pins"""
    inputs_1 = [ Pin(i, Pin.IN, Pin.PULL_DOWN) for i in range(5) ]
    inputs_2 = [ Pin(i, Pin.IN, Pin.PULL_DOWN) for i in range(9, 16) ]

    return inputs_1 + inputs_2

def init_output_pins() -> []:
    """This function initializes the output audio pins"""
    out1 = Pin(19, Pin.OUT)
    out2 = Pin(17, Pin.OUT)
    out3 = Pin(21, Pin.OUT)

    pwm1 = PWM(out1)
    pwm2 = PWM(out2)
    pwm3 = PWM(out3)

    outputs = [ pwm1, pwm2, pwm3 ]

    return outputs

def init_control_pins() -> []:
    """This function initializes the control pins (e.g. scale up/down)"""
    control_pins = [
        Pin(27, Pin.IN, Pin.PULL_DOWN),
        Pin(26, Pin.IN, Pin.PULL_DOWN)
    ]

    return control_pins

def init_out_leds() -> []:
    """This function initializes the output LEDs"""
    return [ Pin(i, Pin.OUT) for i in range(6, 9) ]

def get_note_freq(key_note: int, octave: int) -> int:
    """Returns frequency of played note"""
    note_idx = octave * 12 + key_note
    while note_idx >= len(keys.notes):
        note_idx -= 12
    return keys.notes[note_idx]


def main():
    """Main loop of the program"""
    outs     = init_output_pins()
    keyboard = init_keyboard_pins()
    controls = init_control_pins()
    out_leds = init_out_leds()

    led = Pin("LED", Pin.OUT)
    led.on()

    previously_played = [ -1, -1, -1 ]
    out_idx = 0
    octave = 1

    while True:
        if controls[0].value() == 1:
            octave = max(octave - 1, 0)
            while controls[0].value() == 1:
                pass # wait for button to be depressed
        elif controls[1].value() == 1:
            octave = min(octave + 1, 6)
            sleep(0.5)
            while controls[1].value() == 1:
                pass # wait for button to be depressed

        for idx, k in enumerate(keyboard, start=1):
            if k.value() == 1:
                if idx not in previously_played:
                    # PLAY THE NOTE
                    outs[out_idx].duty_u16(50_000)
                    outs[out_idx].freq(get_note_freq(idx, octave))
                    previously_played[out_idx] = idx
                    out_idx = (out_idx + 1) % 3
            else:
                if idx in previously_played:
                    # STOP THE NOTE
                    index = previously_played.index(idx)
                    outs[index].duty_u16(0)
                    previously_played[index] = -1

            for p_idx, prev in enumerate(previously_played):
                out_leds[p_idx].value(prev != -1)


if __name__ == "__main__":
    main()
